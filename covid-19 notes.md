Today's GA #COVID19 numbers have been released.

3/24 - 1026 cases, 32 deaths

3/23 - 772c, 25d

3/22 - 600c, 23d

3/21 - 507c, 14d

3/20 - 420c, 13d

3/19 - 287c, 10d

3/18 - 197c, 1d

3/17 - 142c, 1d

3/16 - 121c, 1d

3/15 - 99c, 1d

3/14 - 66c, 1d

3/13 - 42c, 1d

3/12 - 31c, 1d

.

.

3/7 - 5c

.

3/5 - 2c

Commentary: DPH issued a disclaimer today that this increase is due to them getting better reporting of test results from outside of the metro area. But honestly the increase is pretty in line with prior days. 

I think the most interesting graph of these right now is the percentage of positive tests when issued by government vs commercial providers. Early on we were led to believe that the government lab was primarily being used for cases where we had strong suspicions, but it's rate has been dropping rapidly. In contrast, commercial tests are catching more and more cases, even though these are theoretically being given out under less stringent standards. It's curious.

Of course, I still want data on test sensitivity and accuracy, and testing per county. 

#StayHome #StayHomeSaveLives

#WashYourHands 

#ActContagious