![](https://thenib.com/wp-content/uploads/2019/08/this-is-not-fine-009-a7b6e6-1.png)

## Daily Summary & Notes

Today’s report uses the data from the [Report from the GA Department of
Public Health](https://dph.georgia.gov/covid-19-daily-status-report).

Today we saw 15885 new confirmed cases (our record is 55849) and 2900
new probable cases (our record is 12883), for at total of 18785 new
cases (our record is 65039). That brings us to 185979 in the past 7 days
(7.9% of total cases so far).

We had 127 new confirmed deaths (our record for new deaths 585) and 23
probable deaths (our record is 70), for a total of 150 deaths (our
record is 480). That brings our total to to 838 in the past 7 days (2.5%
of total deaths so far). We saw 278 new hospitalizations (our record is
1218), bringing our 7-day count to 2698 (2.6% of total hospitalizations
so far). Lastly, we had 22 new ICU admissions (158 is the record),
bringing our 7-day count to 208 (1.4% of total ICU admissions cases so
far).

I realize these numbers can be hard to put into context, so here’s an
alternative metric. In the past 30 days we’ve had equivalent of 221.54
Cruise Ships full of infections, 5.49 747 Crashes in deaths, 46.66 movie
theaters worth of hospitalizations, and 6.42 hotel fulls of ICU
patients.

For testing, we saw 82709 new COVID19 tests, bringing us to 652318 in
the past 7 days (4.1% of total COVID19 tests so far). We also saw 871
new antibody tests, bringing us to 6767 in the past 7 days (0.9% of
total antibody tests so far).

You can access an interactive version of these graphs, including
embedded data [here](https://asb12f.bitbucket.io/COVID19/1-28-22.html).

## Data

### Data Notes

Data does reflect multiple inefficiencies and inaccuracies in the
current reporting system, including showing tests before their results
are returned, delays in reporting on weekends that create artificial
spikes and valleys in change data. In general, interpretation should
examine the general trends, and not focus exclusively on endpoint
trajectories, which are highly influenceable by these data variations.

Beginning July 2021, GA DPH is no longer reporting new data on weekends
or holidays. As a result, these days will show as having zero cases, and
Mondays will likely show significantly elevated numbers.

To help visualize the effects of State actions on the outbreak, I’ve
added a few sets of lines to several of the graphs. The first - the
vertical blue lines - show when the state of emergency went into effect
(3/15; solid line) and when we might expect to see first effects from it
(dotted line). The second - vertical red lines - is the Friday Shelter
in Place was instituted (4/3; solid line) and the date we might expect
to see first effects (dotted line). The third - vertical pink lines -
show when the shelter in place was lifted (4/30; solid line) and the
date we might expect to see first effects (dotted line).

In addition, to help visualize change in graphs using cumulative data
that spans large counts, both linear and algorithmic scales are offered.
You can read more on interpreting graphs using log scales
[here](https://blog.datawrapper.de/weeklychart-logscale/).

Where point data is presented, a LOESS regression with 95% confidence
intervals is shown to help the viewer interpret overall trends in the
data. This is preferred over a line graph connecting all points, which
tends to over-emphasize outliers in report.

### Cumulative Confirmed Cases

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-1-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-2-1.png)

#### Probable Cases

Georgia counts cases that are reported using rapid antigen tests as
“probable” cases rather than “confirmed” cases is they are not
subsequently confirmed by a PCR test. These data have only become
available as of 11/3. As of today, this represents 522171 cases not
included in the total count, which would increase the total by about
28.6% increase. These visualizations show how the total case count would
look if we incorporated that data.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-3-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-4-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-5-1.png)

### Cumulative Hospitalizations

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-6-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-7-1.png)

### Cumulative Deaths

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-8-1.png)

#### Probable Deaths

Georgia counts deaths that occur when a patient only has an antigen
test, or when a patient has clear symptoms of COVID but no PCR is test
is applied before death, as “probable deaths” rather than “confirmed
deaths”. These data have only become available as of 11/3. As of today,
this represents 5366 deaths not included in the total count, which would
increase the total by about 19.5% increase. These visualizations show
how the total case count would look if we incorporated that data.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-9-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-10-1.png)

### Cumulative ICU Use

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-11-1.png)

## Change Patterns

### Count Level Tracking

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-12-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-13-1.png)
![](DailyReport2_files/figure-markdown_github/unnamed-chunk-14-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-15-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-16-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-17-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-18-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-19-1.png)

### Z Score Fluctuations

Because percentage growth becomes misleading over time, I’ve added a
floating 4-week Z-score visualization for each measure to help put into
perspective the magnitude of daily variation in numbers.

For those who don’t spend a lot of time in the world of statistics, a Z
score is a measure that describes the relationship of an observation (in
this case, a particular day’s number) to the average across the entire
group. It is calculated by taking the difference between the observation
and the mean, and dividing by standard deviation.

Z = (Observed Score  - Mean) / Standard Deviation

For example, if the mean score for a group is 50, and the standard
deviation is 10, then a score of 60 woud have a Z score of (60-50) / 10
= 1, and a score of 20 would have a Z score of (20/50) / 10 = -3.

This can be useful in identifying patterns in data reporting, and help
put daily fluctuations in perspective. Because the data is more
localized, it doesn’t fall victim to the diminishing returns effect.
These visualizations are limited to the data from the last 30 days,
which further helps illustrate trends and fluctuations.

#### New Cases

For today’s cases, the 30-day mean is 17528.07 and the standard
deviation is 13316.8.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-20-1.png)

#### Hospitalizations

For today’s hospitalizations, the 30-day mean is 388.83 and the standard
deviation is 206.82.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-21-1.png)

#### Deaths

For today’s deaths, the 30-day mean is 50.97 and the standard deviation
is 37.49.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-22-1.png)

#### ICU Admissions

For today’s ICU Admissions, the 30-day mean is 24.6 and the standard
deviation is 11.47.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-23-1.png)

## Testing

These graphs contain several markers that reflect the changing nature of
the testing data that has been provided over time.

As of 4/28 specific counts of the number of tests administered by the
government and commercial providers stopped being reported.
Additionally, on this date we began to track data on the number of
positive tests conducted by the CDC.

On 5/27, specific counts of serology tests (antibody tests) became
available, which had previously been aggregated into the total test
count. This date has been marked with a vertical gold line on the
graphs. This distinction is important, as positive antibody tests do not
result in new cases in the overall count, and thus both suppress the
positive test rate and artificially inflate estimates of test
prevalence. The daily data for daily COVID19 tests and serology tests is
tracked starting on this date.

### Cumulative Testing

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-24-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-25-1.png)

### Positive Tests by Source

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-26-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-27-1.png)

### Total Testing Trends

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-28-1.png)

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-29-1.png)

For today’s new tests, the 30-day mean is 81420.7 and the standard
deviation is 77876.78.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-30-1.png)

### COVID19 Molecular Testing Trends

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-31-1.png)

For today’s new tests, the 30-day mean is 80473.07 and the standard
deviation is 76886.51.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-32-1.png)

### COVID19 Antibody Testing Trends

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-33-1.png)

For today’s new tests, the 30-day mean is 947.63 and the standard
deviation is 1074.99.

![](DailyReport2_files/figure-markdown_github/unnamed-chunk-34-1.png)

#### Final Thoughts

What do we make of the information from these new graphs? I think there
are a few takeaways. First, it’s safe to say that while the increase in
testing does create an increased ability to detect cases, it is not the
reason that cases are increasing; after all we’re seeing similar
escalations in hospitalizations and deaths which couldn’t be caused by
increased testing. Second, like with the correlations between new tests
and new cases, we can see that there seem to be multiple groupings
within this data, which likely reflect periods of escalated testing in
response to increased cases and changes in how we treat patients
diagnosed with COVID19. Ultimately the story we see here is much richer
and more complex than those who want to blame pandemic numbers on
testing are willing to acknowledge.

## Commentaries

### Updated Masking Guidance (7/27/21)

Today the CDC [has issued new guidance on
masks](https://www.npr.org/sections/health-shots/2021/07/27/1021206558/cdc-expected-to-change-mask-guidance-for-vaccinated-people-including-in-schools),
indicating that fully vaccinated people should continue to wear masks in
a public indoor area in areas with “substantial and high transmission”
of COVID-19. This is based in newer findings on breakthrough cases, and
the significant surge in cases we’re seeing in the unvaccinated
population. I applaud the general idea behind this guidance; however I
suspect that it’s not going to do much to contain the current surge.

Anti-vaxxer and anti-maskers have long used revisions of CDC guidance as
a premise to discourage taking either measure, and to sew uncertainty
about the effectiveness of these public health tools. The new revision,
while based in data, unfortunately gives them more talking points.
People who haven’t followed the data closely will be confused, and that
will cause issues.

More broadly, by directing the guidance to people in areas with
“substantial and high transmission”, the CDC places responsibility on
individuals to assess imminent risk. This is, as I’ve mentioned in the
past, not great. People are bad at assessing risks they can’t see, and
don’t have a good sense of how their media diet shapes and is shaped by
their perception. As a result, most people can’t tell you about hospital
capacity in their area or the level of spread in their vicinity with any
useful accuracy. While they could look up the [CDC’s evaluation of their
county’s transmission
level](https://covid.cdc.gov/covid-data-tracker/#county-view) most
won’t, and are likely to instead look at maps run by their State’s DPH.
In Georgia, the contrast between the two is striking - the CDC shows
almost the entire state as high transmission, while the DPH has most of
the state in light yellow.

All that said, I don’t think we’d see major changes in behavior even if
people were inclined to rigorously check their county’s transmission
levels. While I know many people were concerned that people would lie
about their vaccine status to stop wearing masks, at least here in
Georgia those people weren’t wearing masks to begin with and I doubt
they’ll start with the new guidance. I don’t think there’s the political
will to enforce this policy, nor is there sufficient will to go back to
even the superficial restrictions that were present in the Southeast
during the height of the pandemic.

At this point, I think our best bets are in employer and school
requirements for vaccination, and more rigorous government support to
make it easy for people to access both good information about vaccines
and the vaccines themselves in a single location. Bring vaccination to
where people are, make it visible, and make it easy. Invest in
amplifying public health messaging until it drowns out anti-vaxxers, and
deny them platforms to spread dangerous misinformation. Making items of
public interest contingent on vaccination works - [just look at
France](https://apnews.com/article/europe-business-health-government-and-politics-france-ecc7d4345abf159920bbe36a3b2fefb7).
It’s not without pushback and political cost, but it will save lives.

### Other Commentaries

[Physical Distancing: 3 Feet vs 6
Feet](https://andrewbenesh.medium.com/physical-distancing-3-feet-vs-6-feet-2a5884947bea)

[Is Herd Immunity A Viable Solution to
COVID-19?](https://medium.com/@andrewbenesh/is-herd-immunity-a-viable-solution-to-covid-19-2e4664ab4de4)

[Is Using Common Sense A Viable Solution to
COVID-19?](https://medium.com/@andrewbenesh/untitled-e290fc50f136)

[Should People be Protesting During
COVID19?](https://medium.com/@andrewbenesh/should-people-be-protesting-during-covid19-886416ea640d)

[Making Sense of that Mask
Study](https://link.medium.com/0DbXj2bPW8?fbclid=IwAR0mPR9YE7SEClWiQ4wt-9WkaBj6FJlbK1dwGPeDWr0IOiBbJTIi94Z2Fys)

### Comorbidity (Written 7/15/2020)

I think today is a good time to remind people about comorbidity risks. I
often see people insist that they have no risk because “only people with
pre-existing conditions get COVID”. While pre-existing conditions are
associated with increased risk, this misses both that healthy people
with no prior conditions get COVID, and that what’s counted as
pre-existing conditions is pretty broad. The GA DPH website indicates
that the following are considered comorbid conditions in COVID19 data
reporting: [Chronic Lung Disease, Diabetes Mellitus, Cardiovascular
Disease, Chronic Renal Disease, Chronic Liver Disease, Immunocompromised
Condition, Neurologic/Neurodevelopmental Condition, and
Pregnancy](https://ga-covid19.ondemand.sas.com/docs/GA_COVID19_Dashboard_Guide.pdf).
These are very prevalent conditions here in Georgia - [Over 6.9% of
adults have COPD or other lung
disease](https://www.copdfoundation.org/Portals/0/StateAssessmentCards/SAC__GA_2018.pdf),
[more than 1 in 10 Georgians have
diabetes](https://dph.georgia.gov/diabetes), and [more than 1 in 3
Georgians have some sort of cardiovascular
disease](https://dph.georgia.gov/document/document/heart-disease/download).
I could pull stats fo r the other conditions listed, but the implication
is clear - a large proportion of our citizens are at elevated risk. Most
people likely either have one of these comorbidities, or are close to
someone who does, and don’t recognize the risk.

### Physical Distancing (Written 7/31/2020)

Today I want to talk briefly about social distancing. The guideline
that’s been shared is to maintain 6 feet distance between people.
Unfortunately, many people struggle with this. The struggles tend to
fall into two areas.

First, some people are not good at judging what 6 feet away is; most
people I see are treating 3-4 feet as 6 feet. Often people also may
start at 6 feet away, and slowly close that distance (sometimes
unconsciously). With all this in mind, let me give you a few ways of
thinking about what 6+ feet looks like:

-   If you could shake hands without moving your feet from where they
    are, you’re too close.
-   If you could fall face forward - just straight face planting into
    the ground - and the other person could catch you, you’re too close.
-   If the person could hit you with a baseball bat without leaving
    where they’re standing, you’re too close.

The second issue is that many people interpret “stay at least 6 feet
apart” badly. Much like how people interpret a speed limit of “55MPH” as
“drive 55MPH, if not more”, people interpret social distancing stay “6
feet between persons, if not slightly less”. Aside from making the
absolute minimum safe distance the norm, this also tends to ignore the
reality of human beings as 3D creatures.

Consider the following situation: You have a line 30 feet long in front
of a service counter. How many people can stand in that line and
maintain social distancing? In the abstract, we might quickly calculate
that 30/6 is 5, or even conceptualize 6 if we assume we can put a person
at spot “zero”. Now let’s think about actual human beings. Let’s assume
the first person in line stands 1 foot from the service counter. If we
assume a personal space bubble of about 2 feet, then a minimum of 6 feet
away for the next person is 9 feet from the window; subsequent spots are
at 17 and 25 feet. Suddenly we’re down to 3 people in our socially
distanced line. If some of our “spots” are filled by groups of people,
like families or couples, then we have to build even more space.

With those two issues in mind, I encourage you to think about social
distancing as 10 feet away rather than 6 feet. This accounts for our
poor spatial judgment, tendency to drift closer, and the issues of
humans being 3-dimensional. This also creates space to move in and
around people if you’re working in a classroom or retail environment.

### On Reduced Testing (Written 8/30/2020)

I’d like to spend a little time today talking about a big problem both
here in Georgia and elsewhere regarding information and attitudes about
COVID19. As you may have recently seen in the AJC [demand for COVID19
testing is down in
Georgia](https://www.ajc.com/news/demand-for-virus-tests-falls-in-georgia-as-white-house-pushes-for-more/FQYCYM3YZ5BGDINTKGCVSAAEOQ/).
This is alarming, as Georgia has never really tested at levels
sufficient to contain the virus, and the decrease will only worsen our
ability to monitor and intervene. Decreased testing means more cases
will not be detected, which will increase community spread and further
distort our understanding of the outbreak. So let’s talk about why
testing is going down, and what can be done about it.

One major factor is poor communication about the utility of tests in
recent weeks. Recently the press has become fascinated with stories of
[false positives](https://apnews.com/5d15429433051c78f2ca42fc08fbf835).
This is a real thing that happens, and happens with tests for most
medical conditions; however it’s also pretty uncommon - [studies of PCR
tests suggest a false positive rate below
5%](https://www.health.harvard.edu/blog/which-test-is-best-for-covid-19-2020081020734#:~:text=The%20reported%20rate%20of%20false%20positives%20%E2%80%94%20that%20is%2C%20a%20test,from%20throat%20swabs%20or%20saliva.).
In fact, you’re much, much more likely to get a [false
negative](https://medical.mit.edu/covid-19-updates/2020/06/how-accurate-diagnostic-tests-covid-19),
and that problem of false negatives has [very real human
costs](https://www.statnews.com/2020/08/26/long-haulers-dilemma-many-cannot-prove-they-had-covid19/).
Where clusters of false positives have occurred, they’ve generally been
issues with the labs reporting tests and have been quickly identified.
But unfortunately, this has led to considerable misinformation and
conspiratorial theories spreading. Rather than recognizing that a
national testing campaign may occasionally have errors, COVID deniers
have chosen to interpret the existence of these false positives as proof
that all positive tests are false, spread unsubstantiated claims that
all tests come back positive, and insinuate that asymptomatic cases are
all false positives. For people not versed in the nuances of testing,
this can be dangerously persuasive.

Meanwhile, significant efforts have been underway to blame individuals
for becoming infected and attribute this as a personal fault rather than
acknowledge that our public health response has forced people to risk
unavoidable exposure to the virus or face financial ruin. This can be
seen pretty much everywhere, but it’s particularly visible in
Universities, where administrators have chosen to [bring students to
campus and then shame them for getting
infected](https://www.insidehighered.com/news/2020/08/24/college-covid-strategies-dont-adequately-address-typical-student-behavior),
ignoring the realities of elevated risks that occur in student housing
and poorly ventilated lecture halls and instead blaming parties. While
there is some truth to the concerns about parties, the risks inherent in
university programming and the risks that students assume when they take
on work with the public where they’re often unable to enforce safety
guidelines is a much greater concern. This is essentially just a COVID
variation of the [millenial
shaming](https://www.sfchronicle.com/health/article/Coronavirus-sets-off-generational-shaming-15164185.php)
that’s been a favorite past-time of university administrators for some
years. Rather than making students take more precautions, these
strategies mostly discourage students from testing and reporting, and
makes them less likely to be honest with contact tracers if they do test
positive.

### On Underlying Conditions (Written 8/30/2020)

Yet further discouraging testing is people’s focus on the idea of
underlying conditions. This has been particularly prevalent today,
following news reports saying [“94% of Covid-19 deaths had underlying
medical
conditions”](https://nbc25news.com/news/local/cdc-94-of-covid-19-deaths-had-underlying-medical-conditions).
People are reading this and interpreting it to mean that only 6% of
people who die from COVID would have survived if not for underlying
conditions, and that the risk is wildly overblown.

That would be noteworthy, so let’s look into this a bit. The claim comes
from Table 3 on the [CDC Weekly
Update](https://www.cdc.gov/nchs/nvss/vsrr/covid_weekly/index.htm?fbclid=IwAR3-wrg3tTKK5-9tOHPGAHWFVO3DfslkJ0KsDEPQpWmPbKtp6EsoVV2Qs1Q),
which is captioned:

> Table 3 shows the types of health conditions and contributing causes
> mentioned in conjunction with deaths involving coronavirus disease
> 2019 (COVID-19). For 6% of the deaths, COVID-19 was the only cause
> mentioned. For deaths with conditions or causes in addition to
> COVID-19, on average, there were 2.6 additional conditions or causes
> per death. The number of deaths with each condition or cause is shown
> for all deaths and by age groups.

If we look at the table and associated data, however, we quickly see a
that this is somewhat misleading. What this table is actually doing is
reporting “Conditions Contributing to Deaths where COVID-19 was listed
on the death certificate”. It’s important to understand that [over two
thirds of death certificates list multiple causes of
death](http://dphhs.mt.gov/Portals/85/publichealth/documents/Epidemiology/VSU/VSU_Multiple_Cause_2017.pdf?ver=2017-10-25-171307-117#:~:text=Two%2Dthirds%20of%20death%20certificates,used%20to%20explore%20disease%20interactions.&text=Chronic%20diseases%20such%20as%20Diabetes,of%20multiple%20causes%20of%20death.&text=Injury%20surveillance%20may%20benefit%20from%20multiple%20cause%20analysis.),
and this is generally considered a good thing from a health standpoint -
he inclusion of multiple factors associated with the [death helps us
better understand disease interaction and
progression](https://wonder.cdc.gov/mcd.html). This is different from
reporting the “underlying cause of death”, which is the illness that is
considered to have precipitated the death, which is often difficult to
specify, and which the CDC table does not address. When we consider that
when COVID is fatal, the death is usually a result of [respiratory or
organ failure resulting from damage done to the heart, lungs, liver, and
kidneys](https://www.sciencedaily.com/releases/2020/05/200513081810.htm),
then it makes sense that most COVID19 death certificates would list
things like pneumonia, adult respiratory distress syndrome, respiratory
failure, respiratory arrest, ischemic heart disease, cardiac arrest,
heart failure, renal failure, and sepsis as contributing factors. If we
factor in that [6 in 10 US adults have at least one chronic disease, and
4 in 10 have at least
2](https://www.cdc.gov/chronicdisease/resources/infographic/chronic-diseases.htm),
then it also becomes no surprise that these make an appearance on the
table. And given the promotion of the [Miracle Mineral Solution, AKA
Bleach](https://www.wtsp.com/article/news/regional/florida/miracle-mineral-solution-genesis-ii-church-of-health-and-healing/67-b33b7f2e-2b0c-4853-8434-90732359d730),
it’s not surprising that around 5000 of these deaths seem to be
poisoning related. Taken all together, it’s surprising that any COVID
death certificates don’t list additional contributing factors, let alone
6%!

Because there are conspiracy theorists desperate to capitalize on any
data inconseistency, I should mention that the reason that the table
lists only 161,392 deaths is NOT because the CDC is quietly removing
deaths from the count; it’s because the table relies on limited and
delayed reporting. The national data shows 182,149 deaths and 5,934,824
cases.

As a final note, the recent focus on deaths as the only indicator of
disease threat is a big problem. We are seeing that COVID19 has
significant long term health effects on people who survive it, ranging
from ongoing symptoms of [fatigue, a racing heartbeat, shortness of
breath, achy joints, foggy thinking, a persistent loss of sense of
smell, and damage to the heart, lungs, kidneys, and
brain](https://www.sciencemag.org/news/2020/07/brain-fog-heart-damage-covid-19-s-lingering-problems-alarm-scientists#:~:text=The%20list%20of%20lingering%20maladies,lungs%2C%20kidneys%2C%20and%20brain).
Hospitalized COVID patients are at elevated risk of [blood clots that
can cause strokes, heart attacks, lung blockages, and other
complications](https://www.advisory.com/daily-briefing/2020/06/02/covid-health-effects),
and this is particularly prevalent in young patients. Other residual
symptoms include neurocognitive impairments, and [elevated risk for
psychaitric
disorders](https://www.theguardian.com/world/2020/aug/03/survivors-of-covid-19-show-increased-rate-of-psychiatric-disorders-study-finds).
In children, we’re seeing [Multisystem Inflammatory
Syndrome](https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/children/mis-c.html).
And because myths about children being immune continue to spread, we
should note that [we have evidence of cases and deaths in children as
young as 1 year
old](https://www.cnn.com/2020/08/28/us/georgia-infant-death-coronavirus/index.html).
Lastly, we should remember that we’re only 9 months into this virus, and
we don’t know what the true long term effects will be. Dismissing the
health impacts on survivors is short-sighted and likely to exacerbate
the human suffering and economic damage this virus will cause.

### Final Thoughts

As always, I am not trained in epidemiology, and defer to recognized
experts in the field on all issues. These analyses and commentary are
solely designed to help lay persons approach the publicly available data
and larger public health conversations.

Stay Home.

Wash Your Hands.

Wear a Mask.

## Documentation

Code and data available
[here](https://bitbucket.org/asb12f/covid19-ga/). Analysis conducted
using R.
