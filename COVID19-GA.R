# get data
covid19data <- read.csv("covid19data.csv")
attach(covid19data)

# load functions
source("covid19functions.R")

# do analysis
corona(cases, deaths)
ratechange(change)
newcases(diff)
testingcount(govtests, comtests)
testingpos(govtestspos, comtestspos)
testingrates(govtests, comtests, govtestspos, comtestspos)
infectionmodel(cases)
infectionmodelfuture(cases, moredays=10)
deathmodel(deaths)
deathmodelfuture(deaths, moredays=10)